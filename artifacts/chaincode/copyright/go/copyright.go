/*
SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type SmartContract struct {
	contractapi.Contract
}

type Copyright struct {
	Type   string `json:"type"`
	Author  string `json:"author"`
	Issuedate  string `json:"issuedate"`
	Owner string `json:"owner"`
	Period  int `json:"period"`
	Hash  string `json:"hash"`
}

type QueryResult struct {
	Key    string `json:"Key"`
	Record *Copyright
}

// 初始化账本，添加数字版对象到账本中
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {
	Copyrights := []Copyright{
		Copyright{Type: "music", Author: "liangbo", Issuedate: "2021/10/02 16:34", Owner: "authorMSP", Period: 10, Hash: "fdshgahgrh"},
		Copyright{Type: "picture", Author: "vengao", Issuedate: "2021/10/02 16:35", Owner: "authorMSP", Period: 10, Hash: "srehthswth"},
		Copyright{Type: "vedio", Author: "sihao", Issuedate: "2021/10/02 16:36", Owner: "authorMSP", Period: 10, Hash: "fdssdgaegegegrh"},
		
	}

	for i, copyright := range Copyrights {
		copyrightAsBytes, _ := json.Marshal(copyright)
		err := ctx.GetStub().PutState("COPYRIGHT"+strconv.Itoa(i), copyrightAsBytes)

		if err != nil {
			return fmt.Errorf("Failed to put to world state. %s", err.Error())
		}
	}

	return nil
}

// 创建数字版权对象
func (s *SmartContract) CreateCopyright(ctx contractapi.TransactionContextInterface, copyrightNumber string, cprtype string, author string, owner string, period string, hash string) error {
	
	if s.GetInvokeMSPID(ctx) != "bureauMSP" {
		return fmt.Errorf("you are not the bureau member, and not be allowed to create the copyright")
	}

	issuedate := time.Now().Format("2006/01/02 15:04")
	period1, _ := strconv.Atoi(period)
	
	copyright := Copyright{
		Type:   cprtype,
		Author:  author,
		Issuedate: issuedate,
		Owner: owner,
		Period:  period1,
		Hash:  hash,
	}

	copyrightAsBytes, _ := json.Marshal(copyright)

	return ctx.GetStub().PutState(copyrightNumber, copyrightAsBytes)
}

// 根据ID，查询存储在账本中的指定数字版权对象
func (s *SmartContract) QueryCopyright(ctx contractapi.TransactionContextInterface, copyrightNumber string) (*Copyright, error) {
	copyrightAsBytes, err := ctx.GetStub().GetState(copyrightNumber)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if copyrightAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", copyrightNumber)
	}

	copyright := new(Copyright)
	_ = json.Unmarshal(copyrightAsBytes, copyright)

	return copyright, nil
}

// 查询所有的数字版权信息
func (s *SmartContract) QueryAllCopyrights(ctx contractapi.TransactionContextInterface) ([]QueryResult, error) {
	startKey := ""
	endKey := ""

	resultsIterator, err := ctx.GetStub().GetStateByRange(startKey, endKey)

	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	results := []QueryResult{}

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return nil, err
		}

		copyright := new(Copyright)
		_ = json.Unmarshal(queryResponse.Value, copyright)

		queryResult := QueryResult{Key: queryResponse.Key, Record: copyright}
		results = append(results, queryResult)
	}

	return results, nil
}

//变更数字版权的所有者信息
func (s *SmartContract) ChangeCopyrightOwner(ctx contractapi.TransactionContextInterface, copyrightNumber string, newOwner string) error {
	copyright, err := s.QueryCopyright(ctx, copyrightNumber)

	if err != nil {
		return err
	} else if copyright==nil {
		fmt.Printf("This copyright does not exit")
	}

	if copyright.Owner != s.GetInvokeMSPID(ctx) {
		return fmt.Errorf("you are not the owner--%s--, and not be allowed to change the owner of copyright", copyright.Owner)
	}

	copyright.Owner = newOwner
	copyrightAsBytes, _ := json.Marshal(copyright)

	return ctx.GetStub().PutState(copyrightNumber, copyrightAsBytes)
}

// 删除指定的数字版权对象
func (s *SmartContract) DeleteCopyright(ctx contractapi.TransactionContextInterface, copyrightNumber string) error {	
	copyright, err := s.QueryCopyright(ctx, copyrightNumber)

	if err != nil {
		return err
	} else if copyright==nil {
		fmt.Printf("This copyright does not exit")
	}
	if copyright.Owner != s.GetInvokeMSPID(ctx) {
		return fmt.Errorf("you are not the owner--%s--, and not be allowed to delete the copyright", copyright.Owner)
	}

	return ctx.GetStub().DelState(copyrightNumber)
}

// 获取链码调用对象的mspID
func (s *SmartContract) GetInvokeMSPID(ctx contractapi.TransactionContextInterface) string {	
	

	mspID, _ := ctx.GetClientIdentity().GetMSPID()
	
	return mspID
}

func main() {

	t :=time.Now().Format("2006/01/02 15:04")
	fmt.Printf("Time now is %s\n", t)

	chaincode, err := contractapi.NewChaincode(new(SmartContract))

	if err != nil {
		fmt.Printf("Error create copyright chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting copyright chaincode: %s", err.Error())
	}
}


