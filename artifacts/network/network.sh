#!/bin/bash
# Author: hnu-blockchain team
# Descript: This script is used for start the fabric network, before you use this script, please ensure you are in root user.

MODE="$1"
CHANNEL_NAME="$2"
CC_NAME="$3"
CC_SRC_PATH=${4}
CC_VERSION=${5:-"1.0"}
CC_SEQUENCE=${6:-"1"}

: ${MODE:="up"}
: ${CHANNEL_NAME:="mychannel"}
: ${CC_NAME:="copyright"}
: ${CC_SRC_PATH:="../chaincode/${CC_NAME}/go"}

export PATH=${PWD}/../bin:$PATH
export FABRIC_CFG_PATH=${PWD}/../config

source ./registerEnroll.sh

createCAservise() {
    echo "Build up the CA Servise"
    docker-compose -f ./docker/docker-compose-ca.yaml up -d
}

createCrypto() {
    echo "Generate crypto material using Fabric CA "
    while :
    do
      if [ ! -f "organizations/fabric-ca/author/tls-cert.pem" ]; then
        sleep 2
      else
        break
      fi
    done
    which fabric-ca-client
    if [ "$?" -ne 0 ]; then
    echo "fabric-ca-client tool not found."
    fi
    echo "create Author Idendity"
    createAuthor
    echo "create Buyer1 Idendity"
    createBuyer1
    echo "create Buyer2 Idendity"
    createBuyer2
    echo "create Bureau Idendity"
    createBureau
    echo "create Court Idendity"
    createCourt
    echo "create Orderer Idendity"
    createOrderer
}

createConsortium() {
    echo "CreateConsortium using configtxgen tool"
    which configtxgen
    if [ "$?" -ne 0 ]
    then
    echo "configtxgen tool not found."
    fi
    echo "Generating Orderer Genesis block"
    configtxgen -profile FiveOrgsOrdererGenesis -channelID system-channel -outputBlock ./system-genesis-block/genesis.block
}

networkUp() {
    echo "Start the network"
    if test -e ./system-genesis-block/genesis.block
    then
    docker-compose -f ./docker/docker-compose-copyright-net.yaml up -d
    else
    echo "fail to start the network, genesis block is not exit"
    fi
}

createChanneltx() {
    echo "Start the network"
    which configtxgen
    if [ "$?" -ne 0 ]
    then
    echo "configtxgen tool not found."
    fi
    echo "Generating channel configuration transaction"
    configtxgen -profile FiveOrgsChannel -outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx -channelID $CHANNEL_NAME
}

createAncorPeerTx() {
	for org in author buyer1 buyer2 bureau court
    do
	echo "Generating anchor peer update transaction for ${org}"
	configtxgen -profile FiveOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/${org}MSPanchors.tx -channelID $CHANNEL_NAME -asOrg ${org}MSP
	done
}

createChannel() {
    echo "CreateChannel"
    docker exec -it cli-author /bin/bash -c \
    "peer channel create -o orderer.copyrightnetwork.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/$CHANNEL_NAME.tx --outputBlock ./channel-artifacts/${CHANNEL_NAME}.block \
    --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/ca.crt "
}

joinChannel() {
    while :
    do
      if [ ! -f "./channel-artifacts/${CHANNEL_NAME}.block" ]; then
        sleep 1
      else
        break
      fi
    done
    for org in author buyer1 buyer2 bureau court
    do
    echo "join the ${org} to the ${CHANNEL_NAME}"
    docker exec -it cli-${org} /bin/bash -c \
    "peer channel join -b ./channel-artifacts/${CHANNEL_NAME}.block"
    done
}

updateAnchorPeers() {

    for org in author buyer1 buyer2 bureau court
    do
    if test -e ./channel-artifacts/${org}MSPanchors.tx
    then
    echo "set the Anchor peer for ${org}"
    docker exec -it cli-${org} /bin/bash -c \
    "peer channel update -o orderer.copyrightnetwork.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${org}MSPanchors.tx \
    --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/ca.crt "
    else
    echo "anchor peer configuration file is not exit"
    return
    fi  
    done
    echo "Anchor peers updated  on channel '$CHANNEL_NAME'" 
}


# 打包链码
packageChaincode() {
    peer lifecycle chaincode package ${CC_NAME}_${CC_VERSION}.tar.gz \
    --path $CC_SRC_PATH \
    --lang golang \
    --label ${CC_NAME}_${CC_VERSION} 
    while :
    do
      if [ ! -f "./${CC_NAME}_${CC_VERSION}.tar.gz" ]; then
        sleep 1
      else
        break
      fi
    done
    for org in author buyer1 buyer2 bureau court
    do
    docker cp ${CC_NAME}_${CC_VERSION}.tar.gz cli-${org}:/opt/gopath/src/github.com/hyperledger/fabric/peer/
    done
    echo "Package Chaincode Finished" 
}

# 安装链码
installChaincode() {
    for org in author buyer1 buyer2 bureau court
    do
    echo "Install Chaincode on ${org} peer0"
    docker exec -it cli-${org} /bin/bash -c \
    "peer lifecycle chaincode install ${CC_NAME}_${CC_VERSION}.tar.gz"
    done
    echo "Install Chaincode Finished" 
}

#查询链码
queryInstalled() {
    docker exec -it cli-author /bin/bash -c \
    "peer lifecycle chaincode queryinstalled" \
    >&log.txt

    PACKAGE_ID=$(sed -n "/${CC_NAME}_${CC_VERSION}/{s/^Package ID: //; s/, Label:.*$//; p;}" log.txt)
    echo ${PACKAGE_ID}
}

#批准链码
approveForMyOrg() {
    for org in author buyer1 buyer2 bureau court
    do
    echo "approve Chaincode represente ${org}"
    docker exec -it cli-${org} /bin/bash -c \
    "peer lifecycle chaincode approveformyorg --orderer orderer.copyrightnetwork.com:7050 --channelID ${CHANNEL_NAME} --name ${CC_NAME}  --version ${CC_VERSION} --sequence ${CC_SEQUENCE} --package-id ${PACKAGE_ID} --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/ca.crt "
    done
    echo "Approve Finished" 
}

#检查批准链码状态
checkCommitReadiness() {
    echo "Check commit readiness"
    docker exec -it cli-author /bin/bash -c \
    "peer lifecycle chaincode checkcommitreadiness --channelID ${CHANNEL_NAME} \
    --name ${CC_NAME} --version ${CC_VERSION}  --sequence $CC_SEQUENCE --tls true \
    --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/ca.crt \
    --output json"
}

#提交链码
commitChaincodeDefinition() {
    
    echo "Commit Chaincode"
    docker exec -it cli-author /bin/bash -c \
    "peer lifecycle chaincode commit -o orderer.copyrightnetwork.com:7050 --channelID ${CHANNEL_NAME} --name ${CC_NAME} --version ${CC_VERSION} --sequence $CC_SEQUENCE \
    --peerAddresses peer0.author.copyrightnetwork.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/ca.crt \
    --peerAddresses peer0.buyer1.copyrightnetwork.com:7151 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/ca.crt \
    --peerAddresses peer0.buyer2.copyrightnetwork.com:7251 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/ca.crt \
    --peerAddresses peer0.bureau.copyrightnetwork.com:7351 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/ca.crt \
    --peerAddresses peer0.court.copyrightnetwork.com:7451 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/ca.crt \
    --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/ca.crt "
    echo "Commit Finished" 
    
}

startFabricNetwork() {
    echo "####### START FABRIC NETWORK ######"
    createCAservise
    createCrypto
    createConsortium
    networkUp
    createChanneltx
    createAncorPeerTx
    createChannel
    joinChannel
    updateAnchorPeers
    packageChaincode
    installChaincode
    queryInstalled
    approveForMyOrg
    checkCommitReadiness
    commitChaincodeDefinition
    echo "####### START NETWORK FINISHED ######"
}

stopFabricNetwork() {
    echo "####### STOP FABRIC NETWORK ######"
    docker-compose -f ./docker/docker-compose-ca.yaml down --volumes --remove-orphans
    docker-compose -f ./docker/docker-compose-copyright-net.yaml down --volumes --remove-orphans
    docker volume prune
    rm -rf ./organizations
    rm -rf ./system-genesis-block/*
    rm -rf ./channel-artifacts/*
    echo "####### STOP NETWORK FINISHED ######"
}

network() {
    if test $MODE = "up"
    then
    startFabricNetwork
    elif test $MODE = "down"
    then 
    stopFabricNetwork
    rm -rf ../../express/wallet
    else
    echo "Please input valid instruction"
    fi
}

network


