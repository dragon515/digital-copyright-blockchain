#!/bin/bash


function createAuthor() {

  echo "Enroll the CA admin"
  mkdir -p organizations/peerOrganizations/author.copyrightnetwork.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/
 
  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-author --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-author.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-author.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-author.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-author.pem
    OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/msp/config.yaml

  echo "Register peer0"
  set -x
  fabric-ca-client register --caname ca-author --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register user"
  set -x
  fabric-ca-client register --caname ca-author --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register the author admin"
  set -x
  fabric-ca-client register --caname ca-author --id.name authoradmin --id.secret authoradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  mkdir -p organizations/peerOrganizations/author.copyrightnetwork.com/peers
  mkdir -p organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com

  echo "Generate the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-author -M ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/msp --csr.hosts peer0.author.copyrightnetwork.com --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/msp/config.yaml

  echo "Generate the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-author -M ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls --enrollment.profile tls --csr.hosts peer0.author.copyrightnetwork.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/signcerts/* ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/keystore/* ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/server.key

  mkdir -p ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/msp/tlscacerts/ca.crt

  mkdir -p ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/tlsca
  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/tlsca/tlsca.author.copyrightnetwork.com-cert.pem

  mkdir -p ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/ca
  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/peers/peer0.author.copyrightnetwork.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/ca/ca.author.copyrightnetwork.com-cert.pem

  mkdir -p organizations/peerOrganizations/author.copyrightnetwork.com/users
  mkdir -p organizations/peerOrganizations/author.copyrightnetwork.com/users/User1@author.copyrightnetwork.com

  echo "Generate the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-author -M ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/users/User1@author.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/users/User1@author.copyrightnetwork.com/msp/config.yaml

  mkdir -p organizations/peerOrganizations/author.copyrightnetwork.com/users/Admin@author.copyrightnetwork.com

  echo "Generate the author admin msp"
  set -x
  fabric-ca-client enroll -u https://authoradmin:authoradminpw@localhost:7054 --caname ca-author -M ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/users/Admin@author.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/author/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/author.copyrightnetwork.com/users/Admin@author.copyrightnetwork.com/msp/config.yaml

}

function createBuyer1() {

  echo "Enroll the CA admin"
  mkdir -p organizations/peerOrganizations/buyer1.copyrightnetwork.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7154 --caname ca-buyer1 --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7154-ca-buyer1.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7154-ca-buyer1.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7154-ca-buyer1.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7154-ca-buyer1.pem
    OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/msp/config.yaml

  echo "Register peer0"
  set -x
  fabric-ca-client register --caname ca-buyer1 --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register user"
  set -x
  fabric-ca-client register --caname ca-buyer1 --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register the buyer1 admin"
  set -x
  fabric-ca-client register --caname ca-buyer1 --id.name buyer1admin --id.secret buyer1adminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  mkdir -p organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers
  mkdir -p organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com

  echo "Generate the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7154 --caname ca-buyer1 -M ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/msp --csr.hosts peer0.buyer1.copyrightnetwork.com --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/msp/config.yaml

  echo "Generate the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7154 --caname ca-buyer1 -M ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls --enrollment.profile tls --csr.hosts peer0.buyer1.copyrightnetwork.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/signcerts/* ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/keystore/* ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/server.key

  mkdir -p ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/msp/tlscacerts/ca.crt

  mkdir -p ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/tlsca
  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/tlsca/tlsca.buyer1.copyrightnetwork.com-cert.pem

  mkdir -p ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/ca
  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/peers/peer0.buyer1.copyrightnetwork.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/ca/ca.buyer1.copyrightnetwork.com-cert.pem

  mkdir -p organizations/peerOrganizations/buyer1.copyrightnetwork.com/users
  mkdir -p organizations/peerOrganizations/buyer1.copyrightnetwork.com/users/User1@buyer1.copyrightnetwork.com

  echo "Generate the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:7154 --caname ca-buyer1 -M ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/users/User1@buyer1.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/users/User1@buyer1.copyrightnetwork.com/msp/config.yaml

  mkdir -p organizations/peerOrganizations/buyer1.copyrightnetwork.com/users/Admin@buyer1.copyrightnetwork.com

  echo "Generate the org admin msp"
  set -x
  fabric-ca-client enroll -u https://buyer1admin:buyer1adminpw@localhost:7154 --caname ca-buyer1 -M ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/users/Admin@buyer1.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/buyer1/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/buyer1.copyrightnetwork.com/users/Admin@buyer1.copyrightnetwork.com/msp/config.yaml

}

function createBuyer2() {

  echo "Enroll the CA admin"
  mkdir -p organizations/peerOrganizations/buyer2.copyrightnetwork.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7254 --caname ca-buyer2 --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7254-ca-buyer2.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7254-ca-buyer2.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7254-ca-buyer2.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7254-ca-buyer2.pem
    OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/msp/config.yaml

  echo "Register peer0"
  set -x
  fabric-ca-client register --caname ca-buyer2 --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register user"
  set -x
  fabric-ca-client register --caname ca-buyer2 --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register the buyer2 admin"
  set -x
  fabric-ca-client register --caname ca-buyer2 --id.name buyer2admin --id.secret buyer2adminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  mkdir -p organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers
  mkdir -p organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com

  echo "Generate the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7254 --caname ca-buyer2 -M ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/msp --csr.hosts peer0.buyer2.copyrightnetwork.com --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/msp/config.yaml

  echo "Generate the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7254 --caname ca-buyer2 -M ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls --enrollment.profile tls --csr.hosts peer0.buyer2.copyrightnetwork.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/signcerts/* ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/keystore/* ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/server.key

  mkdir -p ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/msp/tlscacerts/ca.crt

  mkdir -p ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/tlsca
  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/tlsca/tlsca.buyer2.copyrightnetwork.com-cert.pem

  mkdir -p ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/ca
  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/peers/peer0.buyer2.copyrightnetwork.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/ca/ca.buyer2.copyrightnetwork.com-cert.pem

  mkdir -p organizations/peerOrganizations/buyer2.copyrightnetwork.com/users
  mkdir -p organizations/peerOrganizations/buyer2.copyrightnetwork.com/users/User1@buyer2.copyrightnetwork.com

  echo "Generate the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:7254 --caname ca-buyer2 -M ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/users/User1@buyer2.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/users/User1@buyer2.copyrightnetwork.com/msp/config.yaml

  mkdir -p organizations/peerOrganizations/buyer2.copyrightnetwork.com/users/Admin@buyer2.copyrightnetwork.com

  echo "Generate the buyer2 admin msp"
  set -x
  fabric-ca-client enroll -u https://buyer2admin:buyer2adminpw@localhost:7254 --caname ca-buyer2 -M ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/users/Admin@buyer2.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/buyer2/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/buyer2.copyrightnetwork.com/users/Admin@buyer2.copyrightnetwork.com/msp/config.yaml

}

function createBureau() {

  echo "Enroll the CA admin"
  mkdir -p organizations/peerOrganizations/bureau.copyrightnetwork.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7354 --caname ca-bureau --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7354-ca-bureau.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7354-ca-bureau.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7354-ca-bureau.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7354-ca-bureau.pem
    OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/msp/config.yaml

  echo "Register peer0"
  set -x
  fabric-ca-client register --caname ca-bureau --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register user"
  set -x
  fabric-ca-client register --caname ca-bureau --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register the bureau admin"
  set -x
  fabric-ca-client register --caname ca-bureau --id.name bureauadmin --id.secret bureauadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  mkdir -p organizations/peerOrganizations/bureau.copyrightnetwork.com/peers
  mkdir -p organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com

  echo "Generate the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7354 --caname ca-bureau -M ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/msp --csr.hosts peer0.bureau.copyrightnetwork.com --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/msp/config.yaml

  echo "Generate the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7354 --caname ca-bureau -M ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls --enrollment.profile tls --csr.hosts peer0.bureau.copyrightnetwork.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/signcerts/* ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/keystore/* ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/server.key

  mkdir -p ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/msp/tlscacerts/ca.crt

  mkdir -p ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/tlsca
  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/tlsca/tlsca.bureau.copyrightnetwork.com-cert.pem

  mkdir -p ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/ca
  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/peers/peer0.bureau.copyrightnetwork.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/ca/ca.bureau.copyrightnetwork.com-cert.pem

  mkdir -p organizations/peerOrganizations/bureau.copyrightnetwork.com/users
  mkdir -p organizations/peerOrganizations/bureau.copyrightnetwork.com/users/User1@bureau.copyrightnetwork.com

  echo "Generate the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:7354 --caname ca-bureau -M ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/users/User1@bureau.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/users/User1@bureau.copyrightnetwork.com/msp/config.yaml

  mkdir -p organizations/peerOrganizations/bureau.copyrightnetwork.com/users/Admin@bureau.copyrightnetwork.com

  echo "Generate the bureau admin msp"
  set -x
  fabric-ca-client enroll -u https://bureauadmin:bureauadminpw@localhost:7354 --caname ca-bureau -M ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/users/Admin@bureau.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/bureau/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/bureau.copyrightnetwork.com/users/Admin@bureau.copyrightnetwork.com/msp/config.yaml

}

function createCourt() {

  echo "Enroll the CA admin"
  mkdir -p organizations/peerOrganizations/court.copyrightnetwork.com/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7454 --caname ca-court --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7454-ca-court.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7454-ca-court.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7454-ca-court.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7454-ca-court.pem
    OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/msp/config.yaml

  echo "Register peer0"
  set -x
  fabric-ca-client register --caname ca-court --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register user"
  set -x
  fabric-ca-client register --caname ca-court --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register the court admin"
  set -x
  fabric-ca-client register --caname ca-court --id.name courtadmin --id.secret courtadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  mkdir -p organizations/peerOrganizations/court.copyrightnetwork.com/peers
  mkdir -p organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com

  echo "Generate the peer0 msp"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7454 --caname ca-court -M ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/msp --csr.hosts peer0.court.copyrightnetwork.com --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/msp/config.yaml

  echo "Generate the peer0-tls certificates"
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7454 --caname ca-court -M ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls --enrollment.profile tls --csr.hosts peer0.court.copyrightnetwork.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/signcerts/* ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/keystore/* ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/server.key

  mkdir -p ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/msp/tlscacerts/ca.crt

  mkdir -p ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/tlsca
  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/tlsca/tlsca.court.copyrightnetwork.com-cert.pem

  mkdir -p ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/ca
  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/peers/peer0.court.copyrightnetwork.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/ca/ca.court.copyrightnetwork.com-cert.pem

  mkdir -p organizations/peerOrganizations/court.copyrightnetwork.com/users
  mkdir -p organizations/peerOrganizations/court.copyrightnetwork.com/users/User1@court.copyrightnetwork.com

  echo "Generate the user msp"
  set -x
  fabric-ca-client enroll -u https://user1:user1pw@localhost:7454 --caname ca-court -M ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/users/User1@court.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/users/User1@court.copyrightnetwork.com/msp/config.yaml

  mkdir -p organizations/peerOrganizations/court.copyrightnetwork.com/users/Admin@court.copyrightnetwork.com

  echo "Generate the court admin msp"
  set -x
  fabric-ca-client enroll -u https://courtadmin:courtadminpw@localhost:7454 --caname ca-court -M ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/users/Admin@court.copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/court/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/court.copyrightnetwork.com/users/Admin@court.copyrightnetwork.com/msp/config.yaml

}


function createOrderer() {

  echo "Enroll the CA admin"
  mkdir -p organizations/ordererOrganizations/copyrightnetwork.com

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/copyrightnetwork.com
  
  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7554 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7554-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7554-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7554-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7554-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/msp/config.yaml

  echo "Register orderer"
  set -x
  fabric-ca-client register --caname ca-orderer --id.name orderer --id.secret ordererpw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
  { set +x; } 2>/dev/null

  echo "Register the orderer admin"
  set -x
  fabric-ca-client register --caname ca-orderer --id.name ordererAdmin --id.secret ordererAdminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
  { set +x; } 2>/dev/null

  mkdir -p organizations/ordererOrganizations/copyrightnetwork.com/orderers

  mkdir -p organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com

  echo "Generate the orderer msp"
  set -x
  fabric-ca-client enroll -u https://orderer:ordererpw@localhost:7554 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/msp --csr.hosts orderer.copyrightnetwork.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/msp/config.yaml

  echo "Generate the orderer-tls certificates"
  set -x
  fabric-ca-client enroll -u https://orderer:ordererpw@localhost:7554 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls --enrollment.profile tls --csr.hosts orderer.copyrightnetwork.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/server.key

  mkdir -p ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/msp/tlscacerts/tlsca.copyrightnetwork.com-cert.pem

  mkdir -p ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/orderers/orderer.copyrightnetwork.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/msp/tlscacerts/tlsca.copyrightnetwork.com-cert.pem

  mkdir -p organizations/ordererOrganizations/copyrightnetwork.com/users
  mkdir -p organizations/ordererOrganizations/copyrightnetwork.com/users/Admin@copyrightnetwork.com

  echo "Generate the admin msp"
  set -x
  fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@localhost:7554 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/users/Admin@copyrightnetwork.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
  { set +x; } 2>/dev/null

  cp ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/copyrightnetwork.com/users/Admin@copyrightnetwork.com/msp/config.yaml

}



