const  express = require('express')
const routeQuery = require('./routes/routeQuery');
const routeInvoke = require('./routes/routeInvoke');
const routeEnrollAdmin = require('./routes/routeEnrollAdmin');
const routeRegisteUser = require('./routes/routeRegisteUser')


var app = express();

app.use('/query', routeQuery);
app.use('/invoke', routeInvoke);
app.use('/enroll', routeEnrollAdmin);
app.use('/register', routeRegisteUser);

app.listen(3000, function () {
    console.log("The programing is run in 3000 port")
})
