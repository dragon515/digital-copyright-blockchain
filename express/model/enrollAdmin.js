/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const FabricCAServices = require('fabric-ca-client');
const { Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

async function enrollAdmin(org, enrollmentID, enrollmentSecret) {
    try {
        // 加载网络连接配置信息
        const connection_profile = 'connection-'+ org + '.json';
        const ccpPath = path.resolve(__dirname, '..', 'connection-profile', connection_profile);
        const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

        // 创建一个CA客户端用于连接到CA服务器
        const castr = 'ca.' + org + '.copyrightnetwork' + '.com';
        const caInfo = ccp.certificateAuthorities[castr];
        const caTLSCACerts = fs.readFileSync(caInfo.tlsCACerts.path);
        const ca = new FabricCAServices(caInfo.url, { trustedRoots: caTLSCACerts, verify: true }, caInfo.caName);

        // 创建一个基于文件系统的钱包来管理身份
        const walletPath = path.join(__dirname,'..', 'wallet');
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // 检查“钱包”中是否已经存在相应的CA管理员用户证书
        const ca_admin = 'admin' + '_' + org
        const identity = await wallet.get(ca_admin);
        if (identity) {
            console.log(`An identity for the admin user ${ca_admin} already exists in the wallet`);
            return;
        }

        // 注册管理员用户，并将管理员用户证书导入到钱包中
        const enrollment = await ca.enroll({ enrollmentID: enrollmentID, enrollmentSecret: enrollmentSecret });
        const x509Identity = {
            credentials: {
                certificate: enrollment.certificate,
                privateKey: enrollment.key.toBytes(),
            },
            mspId: org + 'MSP',
            type: 'X.509',
        };
        await wallet.put(ca_admin, x509Identity);
        const message = `Successfully enrolled admin user ${ca_admin} and imported it into the wallet`;
        console.log(message);
        return message;

    } catch (error) {
        const errorMessage = `Failed to enroll admin user: ${error}`;
        console.error(errorMessage);
        return errorMessage;
    }
}

module.exports.enrollAdmin = enrollAdmin;


