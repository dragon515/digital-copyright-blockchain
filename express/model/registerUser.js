/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Wallets } = require('fabric-network');
const FabricCAServices = require('fabric-ca-client');
const fs = require('fs');
const path = require('path');


async function registerUser(org) {
    try {
        // 加载连接配置信息
        const connection_profile = 'connection-'+ org + '.json';
        const ccpPath = path.resolve(__dirname, '..', 'connection-profile', connection_profile);
        const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

        // 创建一个CA客户端用于连接到CA服务器
        const castr = 'ca.' + org + '.copyrightnetwork' + '.com';
        const caURL = ccp.certificateAuthorities[castr].url;
        const ca = new FabricCAServices(caURL);

        // 创建一个基于文件系统的钱包来管理身份
        const walletPath = path.join(__dirname,'..', 'wallet');
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // 检查是否已经注册过客户端用户
        const appUser = 'appUser' + '_' + org;
        const userIdentity = await wallet.get(appUser);
        if (userIdentity) {
            console.log(`An identity for the user ${appUser} already exists in the wallet`);
            return;
        }

        // 检查是否已经注册过CA管理员用户
        const ca_admin = 'admin' + '_' + org
        const adminIdentity = await wallet.get(ca_admin);
        if (!adminIdentity) {
            console.log(`An identity for the admin user ${ca_admin} does not exist in the wallet`);
            console.log('Run the enrollAdmin.js application before retrying');
            return;
        }

        const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type); 
        const adminUser = await provider.getUserContext(adminIdentity, 'admin');

        // 登记并注册客户端用户，并将新的用户身份存放到“钱包”
        const secret = await ca.register({
            enrollmentID: appUser,
            enrollmentSecret: "secret",
            role: 'client'
        }, adminUser);
        const enrollment = await ca.enroll({
            enrollmentID: appUser,
            enrollmentSecret: "secret"
        });
        const x509Identity = {
            credentials: {
                certificate: enrollment.certificate,
                privateKey: enrollment.key.toBytes(),
            },
            mspId: org + 'MSP',
            type: 'X.509',
        };
        await wallet.put(appUser, x509Identity);
        const message = `Successfully registered and enrolled admin user ${appUser} and imported it into the wallet`;
        console.log(message);
        return message;

    } catch (error) {
        const errorMessage = `Failed to register user : ${error}`;
        console.error(errorMessage);
        return errorMessage;
    }
}

module.exports.registerUser = registerUser;

// registerUser('author')
