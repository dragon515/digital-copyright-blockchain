/*
 * Copyright IBM Corp. All Rights Reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Gateway, Wallets } = require('fabric-network');
const crypto = require('crypto-js');
const path = require('path');
const fs = require('fs');


// 获取连接到指定Fabric网络的网关对象
async function getGatewayForOrg(org) {
    try {
        // 加载网络配置
        const connection_profile = 'connection-'+ org + '.json';
        const ccpPath = path.resolve(__dirname, '..', 'connection-profile', connection_profile);
        const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

        // 创建一个文件夹用于管理身份
        const walletPath = path.join(__dirname, '..', 'wallet');
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // 检查是否已经注册用户证书
        const appUser = 'appUser' + '_' + org;
        const identity = await wallet.get(appUser);
        if (!identity) {
            console.log(`An identity for the user ${org} does not exist in the wallet`);
            console.log('Run the registerUser.js application before retrying');
            return;
        }

        // 创建一个新的网关对象连接到peer节点
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: appUser, discovery: { enabled: true, asLocalhost: true } });
        return gateway;
        
    } catch (error) {
        console.error(`Failed to get the gateway object: ${error}`);
        process.exit(1);
    }
}

// 获取部署在通道的智能合约对象
async function getContractForOrg(org, channelName, contractName) {
    try {
        // 获取网络对象，该网络对象是链码所在的通道
        const gateway = await getGatewayForOrg(org)
        const network = await gateway.getNetwork(channelName);
        
        // 获取智能合约对象
        const contract = network.getContract(contractName);
        return contract
        
    } catch (error) {
        console.error(`Failed to get the contract object: ${error}`);
        process.exit(1);
    }

}

// 将指定路径的数字版权文件进行hash
async function toHash(copyrightPath) {
    var data = fs.readFileSync(copyrightPath);
    const fshash = crypto.SHA256(data.toString());
    return fshash;
}

module.exports.getContractForOrg = getContractForOrg;
module.exports.getGatewayForOrg = getGatewayForOrg;
module.exports.toHash = toHash;

