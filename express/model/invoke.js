
'use strict';
const {getContractForOrg, toHash} = require('./utils');

async function initLedger(org, channelName, contractName) {
    try {
        // 获取智能合约对象
        const contract = await getContractForOrg(org, channelName, contractName);
        // 调用智能合约
        await contract.submitTransaction('InitLedger');
        console.log(`Transaction has been submitted`);
        return `Transaction has been submitted`;
        
    } catch (error) {
        const errorMessage = `Failed to submmited transaction: ${error}`;
        console.error(errorMessage);
        return errorMessage;
    }
}

async function createCopyright(org, channelName, contractName, copyrightNumber , copyrighType, author, owner, period, copyrightFileName) {
    try {
        // 获取智能合约对象
        const contract = await getContractForOrg(org, channelName, contractName);
        const copyrightHash = await toHash('./copyrightStore/'+ copyrightFileName);
        // 调用智能合约
        await contract.submitTransaction('CreateCopyright', copyrightNumber , copyrighType, author, owner+"MSP", period, copyrightHash);
        console.log(`Transaction has been submitted`);
        return `Transaction has been submitted`;
        
    } catch (error) {
        const errorMessage = `Failed to submmited transaction: ${error}`;
        console.error(errorMessage);
        return errorMessage;
    }
}

async function deleteCopyright(org, channelName, contractName, copyrightNumber) {
    try {
        // 获取智能合约对象
        const contract = await getContractForOrg(org, channelName, contractName);
        // 调用智能合约
        await contract.submitTransaction('DeleteCopyright', copyrightNumber);
        console.log(`Transaction has been submitted`);
        return `Transaction has been submitted`;
        
    } catch (error) {
        const errorMessage = `Failed to submmited transaction: ${error}`;
        console.error(errorMessage);
        return errorMessage;
    }
}

async function changeCopyrightOwner(org, channelName, contractName, copyrightNumber, newOwner) {
    try {
        // 获取智能合约对象
        const contract = await getContractForOrg(org, channelName, contractName);
        // 调用智能合约
        await contract.submitTransaction('changeCopyrightOwner', copyrightNumber, newOwner+"MSP");
        console.log(`Transaction has been submitted`);
        return `Transaction has been submitted`
        
    } catch (error) {
        const errorMessage = `Failed to submmited transaction: ${error}`;
        console.error(errorMessage);
        return errorMessage;
    }
}

module.exports.createCopyright = createCopyright;
module.exports.changeCopyrightOwner = changeCopyrightOwner;
module.exports.initLedger = initLedger;
module.exports.deleteCopyright = deleteCopyright;

// initLedger('author', 'mychannel', 'copyright');
// createCopyright('author', 'mychannel', 'copyright', 'COPYRIGHT9', 'music', 'author', 'author', '16', '../test.txt')
// deleteCopyright('author', 'mychannel', 'copyright', 'COPYRIGHT9')
