
'use strict';
const {getContractForOrg} = require('./utils');

async function queryAllCopyrights(org, channelName, contractName) {
    try {
        // 获取智能合约对象
        const contract = await getContractForOrg(org, channelName, contractName);
        // 调用智能合约
        const result = await contract.evaluateTransaction('QueryAllCopyrights');
        console.log(`Transaction has been evaluated, result is:`);
        console.log(result.toString())
        return result.toString()
        
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        process.exit(1);
    }
}

async function queryCopyright(org, channelName, contractName, copyrightNumber) {
    try {
        // 获取智能合约对象
        const contract = await getContractForOrg(org, channelName, contractName);
        // 调用智能合约
        const result = await contract.evaluateTransaction('QueryCopyright', copyrightNumber);
        console.log(`Transaction has been evaluated, result is:`);
        console.log(result.toString())
        return result.toString()
        
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        process.exit(1);
    }
}

module.exports.queryAllCopyrights = queryAllCopyrights;
module.exports.queryCopyright = queryCopyright;


