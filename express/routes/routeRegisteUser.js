var express = require('express');
var register = require('../model/registerUser');
var router = express.Router();
 
router.post('/:org', function(req, res) {
  try {
    const args = ["org"];
    const params = Object.keys(req.params);
    const values = Object.values(req.params);
    if (args.length != params.length)
    {
      res.send("The number of params is not suit the registerUser function");
    }
    for (let i=0; i< args.length; i++)
    {
      if (args[i] != params[i])
      {
        res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
      }

    }
    register.registerUser(values[0]).then(function(result){res.send(result)});
  } catch (error) {
    console.error(`Failed to registe User: ${error}`);
    res.send(`${error}`)
  }
});

module.exports = router;
