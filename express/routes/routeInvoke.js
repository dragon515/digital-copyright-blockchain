var express = require('express');
var router = express.Router();
var invoke = require('../model/invoke');


router.post('/initLedger/:org/:channelName/:contractName', function(req, res) {
   try {
      const args = ["org", "channelName", "contractName"];
      const params = Object.keys(req.params);
      const values = Object.values(req.params);
      if (args.length != params.length)
      {
        res.send("The number of params is not suit the initLedger function");
      }
      for (let i=0; i< args.length; i++)
      {
        if (args[i] != params[i])
        {
          res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
        }
  
      }
      invoke.initLedger(values[0], values[1], values[2]).then(function(result){res.send(result)});
    } catch (error) {
      console.error(`Failed to submit transaction: ${error}`);
      res.send(`${error}`)
  
    }
 });

router.post('/createCopyright/:org/:channelName/:contractName/:copyrightNumber/:copyrightType/:author/:owner/:period/:copyrightPath', function(req, res) {
   try {
      const args = ["org", "channelName", "contractName", "copyrightNumber", "copyrightType", "author", "owner", "period", "copyrightPath"];
      const params = Object.keys(req.params);
      const values = Object.values(req.params);
      if (args.length != params.length)
      {
        res.send("The number of params is not suit the createCopyright function");
      }
      for (let i=0; i< args.length; i++)
      {
        if (args[i] != params[i])
        {
          res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
        }
  
      }
      invoke.createCopyright(values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8]).then(function(result){res.send(result)});
    } catch (error) {
      console.error(`Failed to submit transaction: ${error}`);
      res.send(`${error}`)
  
    }
});

router.put('/changeCopyrightOwner/:org/:channelName/:contractName/:copyrightNumber/:newOwner', function(req, res) {
   try {
      const args = ["org", "channelName", "contractName", "copyrightNumber", "newOwner"];
      const params = Object.keys(req.params);
      const values = Object.values(req.params);
      if (args.length != params.length)
      {
        res.send("The number of params is not suit the changeCopyrightOwner function");
      }
      for (let i=0; i< args.length; i++)
      {
        if (args[i] != params[i])
        {
          res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
        }
  
      }
      invoke.changeCopyrightOwner(values[0], values[1], values[2], values[3], values[4]).then(function(result){res.send(result)});
    } catch (error) {
      console.error(`Failed to submit transaction: ${error}`);
      res.send(`${error}`)
  
    }
});

router.delete('/deleteCopyright/:org/:channelName/:contractName/:copyrightNumber', function(req, res) {
   try {
      const args = ["org", "channelName", "contractName", "copyrightNumber"];
      const params = Object.keys(req.params);
      const values = Object.values(req.params);
      if (args.length != params.length)
      {
        res.send("The number of params is not suit the deleteCopyright function");
      }
      for (let i=0; i< args.length; i++)
      {
        if (args[i] != params[i])
        {
          res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
        }
  
      }
      invoke.deleteCopyright(values[0], values[1], values[2], values[3]).then(function(result){res.send(result)});
    } catch (error) {
      console.error(`Failed to submit transaction: ${error}`);
      res.send(`${error}`)
  
    }
});

module.exports = router;
