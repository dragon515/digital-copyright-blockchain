var express = require('express');
var enroll = require('../model/enrollAdmin');
var router = express.Router();
 
router.post('/:org/:enrollmentID/:enrollmentSecret', function(req, res) {
  try {
    const args = ["org", "enrollmentID", "enrollmentSecret"];
    const params = Object.keys(req.params);
    const values = Object.values(req.params);
    if (args.length != params.length)
    {
      res.send("The number of params is not suit the enrollAdmin function");
    }
    for (let i=0; i< args.length; i++)
    {
      if (args[i] != params[i])
      {
        res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
      }

    }
    enroll.enrollAdmin(values[0], values[1], values[2]).then(function(result){res.send(result)});
  } catch (error) {
    console.error(`Failed to enroll Admin: ${error}`);
    res.send(`${error}`)
  }
});

module.exports = router;
