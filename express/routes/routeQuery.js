var express = require('express');
var query = require('../model/query');
var router = express.Router();
 
router.get('/queryAllCopyrights/:org/:channelName/:contractName', function(req, res) {
  try {
    const args = ["org", "channelName", "contractName"];
    const params = Object.keys(req.params);
    const values = Object.values(req.params);
    if (args.length != params.length)
    {
      res.send("The number of params is not suit the queryAllCopyrights function");
    }
    for (let i=0; i< args.length; i++)
    {
      if (args[i] != params[i])
      {
        res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
      }

    }
    query.queryAllCopyrights(values[0], values[1], values[2]).then(function(result){res.send(result)});
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    res.send(`${error}`)

  }
});
router.get('/queryCopyright/:org/:channelName/:contractName/:copyrightNumber', function (req, res) {
  try {
    const args = ["org", "channelName", "contractName", "copyrightNumber"];
    const params = Object.keys(req.params);
    const values = Object.values(req.params);
    if (args.length != params.length)
    {
      res.send("The number of params is not suit the queryCopyright function");
    }
    for (let i=0; i< args.length; i++)
    {
      if (args[i] != params[i])
      {
        res.send(`Number ${i+1} param must be ${args[i]}, but the provided is ${params[i]}`);
      }

    }
    query.queryCopyright(values[0], values[1], values[2], values[3]).then(function(result){res.send(result)});
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    res.send(`${error}`)

  }
});

module.exports = router;
